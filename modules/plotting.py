import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import ROOT

from modules.samples import l_bkg

# Set seaborn style
sns.set()
sns.set_style("ticks")
colpal = sns.color_palette()
c_blue = colpal[0]
c_orange = colpal[1]
c_green = colpal[2]
c_red = colpal[3]

# Color Brewer palettes (reversed, i.e. from dark to light)
red_pal = sns.color_palette('Reds_r', n_colors=3)
orange_pal = sns.color_palette('Oranges_r', n_colors=3)
green_pal = sns.color_palette('Greens_r', n_colors=3)
blue_pal = sns.color_palette('Blues_r', n_colors=3)
purple_pal = sns.color_palette('Purples_r', n_colors=3)
grey_pal = sns.color_palette('Greys_r', n_colors=3)
RdPu_pal = sns.color_palette('RdPu_r', n_colors=3)

# Map sample groups to a color
d_bkg_col = {'ttbar':     blue_pal[0],
             'singleTop': blue_pal[1],
             'topOther':  blue_pal[2],
             'lowMassDY': green_pal[1],
             'Zjets':     green_pal[0],
             'Wjets':     green_pal[2],
             'diboson':   orange_pal[1],
             'triboson':  orange_pal[2],
             'higgs':     RdPu_pal[0],
             }

# List of bkg colors ordered according to the order in l_bkg
l_bkg_col = [d_bkg_col[bkg] for bkg in l_bkg]


def plotTrainTestOutput(d_X_train_bkg, d_ew_train_bkg, X_train_sig, ew_train_sig, d_X_test_bkg, d_ew_test_bkg, X_test_sig, ew_test_sig, signal_region, train_even):
  
  n_classes = d_X_train_bkg['Zjets'].shape[1]

  if n_classes is 1:
    l_X_train_bkg_arrs = [d_X_train_bkg[i_bkg]['output'].squeeze() for i_bkg in l_bkg]
    l_X_test_bkg_arrs = [d_X_test_bkg[i_bkg]['output'].squeeze() for i_bkg in l_bkg]
  else:
    l_X_train_bkg_arrs = [[d_X_train_bkg[i_bkg]['output'+str(i_class)] for i_bkg in l_bkg] for i_class in range(n_classes)]
    l_X_test_bkg_arrs = [[d_X_test_bkg[i_bkg]['output'+str(i_class)] for i_bkg in l_bkg] for i_class in range(n_classes)]

  if d_ew_train_bkg is not None:
    l_ew_train_bkg_arrs = [d_ew_train_bkg[i_bkg] for i_bkg in l_bkg]
  else:
   l_ew_train_bkg_arrs = None
  if d_ew_test_bkg is not None:
    l_ew_test_bkg_arrs = [d_ew_test_bkg[i_bkg] for i_bkg in l_bkg]
  else:
    l_ew_test_bkg_arrs = None

  signal_group_label = 'C1N2 '+ signal_region
  if 'low' in signal_region:
    signal_label = '(200, 100) GeV'
  elif 'int' in signal_region:
    signal_label = '(500, 200) GeV'
  elif 'high' in signal_region:
    signal_label = '(600, 0) GeV'

  bins = np.linspace(0., 1., 41)
  l_classes = ['Signal', 'Zjets', 'Diboson']

  fig, ax = plt.subplots(n_classes, 1, figsize=[6.4, 4.8*n_classes])
  for i_class in range(n_classes):

    if n_classes > 1:
      ax = ax[i_class]
      
      l_X_train_bkg_arrs = l_X_train_bkg_arrs[i_class]
      l_X_test_bkg_arrs = l_X_test_bkg_arrs[i_class]
      X_train_sig = X_train_sig['output'+str(i_class)]
      X_test_sig = X_test_sig['output'+str(i_class)]

      ax.set_xlabel(l_classes[i_class]+' probability')
    else:
      ax.set_xlabel('Signal probability')

    ax.set_yscale('symlog')
    ax.set_ylim([0, 1E7])
    ax.set_ylabel('Entries')

    n_train_bkg = sum(X_train_bkg.size for X_train_bkg in l_X_train_bkg_arrs)
    n_test_bkg = sum(X_test_bkg.size for X_test_bkg in l_X_test_bkg_arrs)
    n_train_sig = len(X_train_sig)
    n_test_sig = len(X_test_sig)
    sf_train_bkg = n_test_bkg/n_train_bkg
    sf_train_sig = n_test_sig/n_train_sig

    ax.hist(l_X_train_bkg_arrs,
             bins=bins,
             density=False,
             weights=[sf_train_bkg*i_ew_bkg_arr for i_ew_bkg_arr in l_ew_train_bkg_arrs],
             histtype='stepfilled',
             #color=colpal[:len(l_bkg)][::-1],
             color=l_bkg_col,
             alpha=0.75,
             label=l_bkg,
             stacked=True,
             linewidth=2)

    ax.hist(X_train_sig.squeeze(),
             bins=bins,
             density=False,
             weights=sf_train_sig*ew_train_sig,
             histtype='step',
             color=c_red,
             alpha=0.75,
             label=signal_group_label,
             stacked=False,
             linewidth=2,
             #hatch='//'
             )

    ax.hist(l_X_test_bkg_arrs,
             bins=bins,
             density=False,
             weights=l_ew_test_bkg_arrs,
             histtype='step',
             #color=colpal[:len(l_bkg)][::-1],
             color=l_bkg_col,
             alpha=1,
             label=l_bkg,
             stacked=True,
             linewidth=2,
             linestyle='--')

    ax.hist(X_test_sig.squeeze(),
             bins=bins,
             density=False,
             weights=ew_test_sig,
             histtype='step',
             color=c_red,
             alpha=1,
             label=signal_label,
             stacked=False,
             linewidth=2,
             linestyle='--')

    handles, labels = plt.gca().get_legend_handles_labels()
    leg_train = mpatches.Patch(color='black', linestyle='solid', fill=True, alpha=0.75, label='train')
    leg_test = mpatches.Patch(color='black', linestyle='dashed', fill=False, alpha=0.75, label='test')

    leg1 = ax.legend(
                     handles=[handles[0], handles[1], handles[2], handles[3], handles[4], handles[5], handles[6], handles[7], handles[8], handles[9]], 
                     labels=[labels[0], labels[1], labels[2], labels[3], labels[4], labels[5], labels[6], labels[7], labels[8], labels[9]], 
                     ncol=1,
                     loc='upper left',
                     frameon=False,
                     #title='Train vs. test',
                     bbox_to_anchor=(1.05, 0.8),
                     borderaxespad=0.
                     )

    leg2 = ax.legend(
                     handles=[leg_train, leg_test], 
                     labels=["train", "test"],
                     ncol=1,
                     loc='upper left',
                     frameon=False,
                     #title='Train vs. test',
                     bbox_to_anchor=(1.05, 1),
                     borderaxespad=0.
                     )

    ax.add_artist(leg1)

    #plt.rc('text', usetex=True)
    #props = dict('')
    
    if train_even == 0:
      ax.text(0.1, 0.8, '13 TeV, ~70/fb\nTrained (tested) on odd (even) run numbers', transform=ax.transAxes)
    elif train_even == 1:
      ax.text(0.1, 0.8, '13 TeV, ~70/fb\nTrained (tested) on even (odd) run numbers', transform=ax.transAxes)

  return leg1


def plotFinalTestOutput(d_X_test_bkg, d_ew_test_bkg, X_test_sig, ew_test_sig, X_test_data, signal_region, train_even, figure_text=''):

  n_classes = d_X_test_bkg['Zjets'].shape[1]

  if n_classes is 1:
    l_X_test_bkg_arrs = [d_X_test_bkg[i_bkg].output for i_bkg in l_bkg]
  else:
    l_X_test_bkg_arrs = [[d_X_test_bkg[i_bkg]['output'+str(i_class)] for i_bkg in l_bkg] for i_class in range(n_classes)]

  if d_ew_test_bkg is not None:
    l_ew_test_bkg_arrs = [d_ew_test_bkg[i_bkg] for i_bkg in l_bkg]
  else:
    l_ew_test_bkg_arrs = None

  signal_group_label = 'C1N2 '+ signal_region
  if 'low' in signal_region:
    signal_label = '(200, 100) GeV'
  elif 'int' in signal_region:
    signal_label = '(500, 200) GeV'
  elif 'high' in signal_region:
    signal_label = '(600, 0) GeV'

  bins = np.linspace(0.5, 1., 51)
  #l_classes = ['Signal', 'Zjets', 'Diboson']

  #fig, ax = plt.subplots(n_classes, 1, figsize=[6.4, 4.8*n_classes])
  #for i_class in range(n_classes):

  #  if n_classes > 1:
  #    ax = ax[i_class]
  #    
  #    l_X_test_bkg_arrs = l_X_test_bkg_arrs[i_class]
  #    X_test_sig = X_test_sig['output'+str(i_class)]

  #    ax.set_xlabel(l_classes[i_class]+' probability')
  #  else:
  #    ax.set_xlabel('Signal probability')

  #  ax.set_yscale('symlog')
  #  ax.set_ylim([0, 1E7])
  #  ax.set_ylabel('Entries')

  fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, gridspec_kw={'height_ratios': [3, 1], 'hspace': 0.1}, figsize=[6.4, 4.8*(4/3)])

  ax1.set_yscale('symlog')
  ax1.set_ylim([0, 1E7])
  ax1.set_ylabel('Entries')

  bkg_hist, bkg_hist_bins, bkg_hist_patches = ax1.hist(l_X_test_bkg_arrs,
           bins=50, #bins,
           range=(0., 1.),
           weights=l_ew_test_bkg_arrs,
           histtype='stepfilled',
           #color=colpal[:len(l_bkg)][::-1],
           color=l_bkg_col,
           alpha=0.75,
           label=l_bkg,
           stacked=True,
           linewidth=2,
           linestyle='-')

  sig_hist, sig_hist_bins, sig_hist_patches = ax1.hist(X_test_sig.squeeze(),
           bins=50, #bins,
           range=(0., 1.),
           weights=ew_test_sig,
           histtype='step',
           color=c_red,
           alpha=1,
           label=signal_label,
           stacked=False,
           linewidth=2,
           linestyle='-')

  data_counts, bin_edges = np.histogram(X_test_data, bins=50, range=(0., 1.))
  bin_centers = (bin_edges[:-1] + bin_edges[1:])/2.
  data_err = np.sqrt(data_counts)

  data_hist, data_hist_bins, data_hist_patches = ax1.errorbar(bin_centers,
           data_counts,
           yerr=data_err,
           fmt='o',
           mfc='black',
           ecolor='black',
           label='data')

  ax2.set_xlabel('Signal probability')
  ax2.set_yscale('linear')
  y_max = 3
  ax2.set_ylim([0, y_max])
  ax2.locator_params(nbins=y_max)
  ax2.set_ylabel('Significance Z')

  print("bkg_hist.shape", np.array(bkg_hist).shape)
  print("sig_hist.shape", np.array(sig_hist).shape)
  Zn = ROOT.RooStats.NumberCountingUtils.BinomialExpZ
  Zn_values = np.array([ Zn( np.array(sig_hist)[i_bin:].sum(), np.array(bkg_hist)[:,i_bin:].sum(), 0.3) for i_bin in range(len(bins)-1) ])

  print("Zn_values.shape", Zn_values.shape)
  min_value = 0.7
  ax2.hist(bins[:-1],
           bins=50, #bins,
           range=(0.5, 1.),
           weights=Zn_values,
           histtype='step',
           color=c_red,
           alpha=1,
           label=signal_label,
           stacked=False,
           linewidth=2,
           linestyle='-')

  #handles, labels = plt.gca().get_legend_handles_labels()
  #leg_train = mpatches.Patch(color='black', linestyle='solid', fill=True, alpha=0.75, label='train')
  #leg_test = mpatches.Patch(color='black', linestyle='dashed', fill=False, alpha=0.75, label='test')
  leg = ax1.legend(
            #handles=[leg_train, leg_test, handles[9], handles[8], handles[7], handles[6], handles[5], handles[4], handles[3], handles[2], handles[1], handles[0]], 
            #labels=["train", "test", labels[9], labels[8], labels[7], labels[6], labels[5], labels[4], labels[3], labels[2], labels[1], labels[0]], 
            ncol=1,
            loc='upper left',
            frameon=False,
            #title='Test set',
            bbox_to_anchor=(1.05, 1),
            borderaxespad=0.)

  #plt.rc('text', usetex=True)

  if train_even == 0:
    ax1.text(0.1, 0.7, 'ATLAS work in progress\n13 TeV, ~70/fb\n\nTrained (tested) on odd (even) run numbers', transform=ax1.transAxes)
  elif train_even == 1:
    ax1.text(0.1, 0.75, "ATLAS work in progress\n13 TeV, ~70/fb\n\nTrained (tested) on even (odd) run numbers", transform=ax1.transAxes)
  elif train_even == 2:
    ax1.text(0.1, 0.8, 'ATLAS work in progress\n13 TeV, 139/fb', transform=ax1.transAxes)

  return leg


def plot_learning_curve(train_sizes, train_scores, valid_scores):
  if train_scores.ndim is 1:
    train_scores_lc_mean = np.mean(train_scores)
    train_scores_lc_std = np.std(train_scores)
    valid_scores_lc_mean = np.mean(valid_scores)
    valid_scores_lc_std = np.std(valid_scores)
  else:
    train_scores_lc_mean = np.mean(train_scores, axis=1)
    train_scores_lc_std = np.std(train_scores, axis=1)
    valid_scores_lc_mean = np.mean(valid_scores, axis=1)
    valid_scores_lc_std = np.std(valid_scores, axis=1)

  print("train_sizes.shape", train_sizes.shape)
  print("train_scores.shape", train_scores.shape)
  print("train_scores", train_scores)
  print("train_scores_lc_mean.shape", train_scores_lc_mean.shape)

  # Plot learning curves
  figG, axsG = plt.subplots()
  # 68% CL bands
  axsG.fill_between( train_sizes, train_scores_lc_mean - train_scores_lc_std, train_scores_lc_mean + train_scores_lc_std, alpha=0.2, color="r", lw=2)
  axsG.fill_between( train_sizes, valid_scores_lc_mean - valid_scores_lc_std, valid_scores_lc_mean + valid_scores_lc_std, alpha=0.2, color="g", lw=2)
  # Training and validation scores
  axsG.plot( train_sizes, train_scores_lc_mean, 'o-', label="Training score", color="r", lw=2)
  axsG.plot( train_sizes, valid_scores_lc_mean, 'o-', label="Cross-validation score", color="g", lw=2)
  axsG.set_xlabel("Training examples")
  axsG.set_ylabel('Score')
  axsG.legend(loc="best")
  axsG.set_title('Learning curves')
  #axsG.set_ylim(0., 1.)
  plt.savefig(output_dir + 'learning_curve.pdf')
  #plt.show()

