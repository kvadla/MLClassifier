python main.py \
    --nn \
    --read_hdf5 \
    --signal_region 'low-2J' \
    --train_even \
    --balanced 0 \
    --n_hidden_layers 4 \
    --n_nodes 100 \
    --lr 1e-4 \
    --batch_size 32 \
    --epochs 1000 \
    --dropout 0.1 \
    --L1 1e-3 \
    --L2 1e-3
